# PyTorch Pipeline

A simple PyTorch pipeline for training and serving deep neural networks.


## Setup
Set up an environment with the Python version found in `.python-version` in this project.
After the correct Python version has been installed you may run the following to install relevant Python packages:
```
$ pip install -r  requirements.txt
```

## Training 
To run training run the following from your CLI:
```
$ python run_trainer.py
```


## Build
To build the container run the following:
```
$ docker build -f Dockerfile -t ${image_name} .
```
Where you'll replace `${image_name}` with your chosen name for the container.

## Serving
To run the app locally after the container has been built, you may run:
```
sh serve.sh
```
