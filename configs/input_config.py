"""
Config file for input data used in model.
"""


class InputConfig:
    input_path = "input/"

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
