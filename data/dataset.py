"""
Program for setting up training data.
"""


import torch
from torch.utils.data import Dataset


class TrainDataset(Dataset):
    """
    Training dataset
    """
    def __init__(self, features, labels):
        """
        Args:
            features (numpy.array):
            labels (numpy.array):
        """
        super().__init__()
        self.features = torch.from_numpy(features)
        self.labels = torch.from_numpy(labels)
        self.n_samples = features.shape[0]

    def __getitem__(self, idx: int):
        """
        Args:
            idx: idx for record to return.
        Returns:
            Tuple of features and labels.
        """
        features_batch = self.features[idx].float()
        labels_batch = self.labels[idx].float()
        return features_batch, labels_batch

    def __len__(self):
        """
        Returns:
            Number of samples in the dataset.
        """
        return self.n_samples
