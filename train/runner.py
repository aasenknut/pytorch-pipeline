from configs.model_config import ModelConfig, TrainerConfig
from train.trainer import Trainer
from train.network import Net
from data.dataset import TrainDataset
from data.data_builder import train_data


def run_training():
    net = Net(
        input_size = ModelConfig.input_size,
        output_size = ModelConfig.output_size,
        lin1_size = ModelConfig.lin1_size,
        dropout_p = ModelConfig.dropout_p,
    )
    features, labels = train_data()
    ds = TrainDataset(features, labels)
    fit = Trainer(
        model = net,
        config = TrainerConfig
    ).fit(ds)
    fit.write(TrainerConfig.torch_save_path)
