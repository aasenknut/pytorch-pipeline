from torch import nn


class Net(nn.Module):
    """Implements a simple MLP to test the trainer.

    Args:
        input_size (int): Number of features to input into network.
        output_size (int): Number of output nodes.
        lin1_features (int): Number nodes in linear layer.
        droput_p (float): Dropout probability.
    """
    def __init__(self, input_size, output_size, lin1_size, dropout_p):
        super().__init__()
        # 1. layer
        self.lin1 = nn.Linear(in_features=input_size, out_features=lin1_size)
        self.relu1 = nn.ReLU()
        self.dropout1 = nn.Dropout(p=dropout_p)

        # 2. layer
        self.lin2 = nn.Linear(in_features=lin1_size, out_features=5)
        self.relu2 = nn.ReLU()
        self.dropout2 = nn.Dropout(p=dropout_p)

        # 3. layer
        self.lin3 = nn.Linear(in_features=5, out_features=output_size)
        self.sig = nn.Sigmoid()

    def forward(self, x):

        x = self.lin1(x)
        x = self.relu1(x)
        x = self.dropout1(x)

        x = self.lin2(x)
        x = self.relu2(x)
        x = self.dropout2(x)

        x = self.lin3(x)
        x = self.sig(x)

        return x
